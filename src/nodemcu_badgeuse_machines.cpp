// Nodemcu - RFID-RC522
// D2      - RST
// D5      - SCK
// D6      - MISO
// D7      - MOSI
// D8      - SDA
// GND     - GND
// 3.3     - 3.3
#include <Arduino.h>
#include <ESP8266WiFi.h>

#define RST_PIN 5
#define SS_PIN 15

#include <SPI.h>
#include <MFRC522.h>

MFRC522 mfrc522(SS_PIN, RST_PIN);   // Create MFRC522 instance.
int status = 0;
int out = 0;
//trolollo

//<<<WiFiClient
WiFiClient client;

void setup()
{
  Serial.begin(115200);

  pinMode(4, OUTPUT); //Led
  pinMode(0, OUTPUT); //Led
  digitalWrite(4, HIGH);   // turn the red LED on

// RFID
  SPI.begin();      // Initiate  SPI bus
  mfrc522.PCD_Init();   // Initiate MFRC522

// connect to the wifi
    WiFi.disconnect();
    delay(3000);
    Serial.println("Connecting to the wifi");
    WiFi.begin("sqylab_wifi","arduino2019");
    while ((!(WiFi.status() == WL_CONNECTED))){
        delay(300);
        Serial.print(".");}
    Serial.println("Connected");
    Serial.println("Your IP is");
    Serial.println((WiFi.localIP().toString()));
    Serial.println("Force du signal : (50=bon, 100=mauvais)");
    Serial.println(WiFi.RSSI());

}


void loop()
{
  // Look for new cards
  if ( ! mfrc522.PICC_IsNewCardPresent())
  {
    return;
  }
  // Select one of the cards
  if ( ! mfrc522.PICC_ReadCardSerial())
  {
    return;
  }
  //Show UID on serial monitor
  Serial.println();
  Serial.print(" UID tag :");
  String rfid = "";
  String jsonStart = "rfid=";
  String jsonStop = "";
  String json = "";

  for (byte i = 0; i < mfrc522.uid.size; i++)
  {
     Serial.print(mfrc522.uid.uidByte[i] < 0x10 ? " 0" : " ");
     Serial.print(mfrc522.uid.uidByte[i], HEX);
     rfid.concat(String(mfrc522.uid.uidByte[i] < 0x10 ? " 0" : " "));
     rfid.concat(String(mfrc522.uid.uidByte[i], HEX));
  }
  Serial.println();
  rfid.toUpperCase();
  status = 1;

  //send request via wifi
  if (client.connect("192.168.1.2", 80)) {
    String p;

    json.concat(jsonStart);
    json.concat(rfid);
    json.concat(jsonStop);

    p.reserve(256);
    p.concat("PUT /rfid/  HTTP/1.1\r\n");
    p.concat("Host: 192.168.1.2\r\n");
    p.concat("User-Agent: lecteur-rfid/0.0.1\r\n");
    p.concat("Accept: */*\r\n");
    p.concat("Content-Length: ");
    p.concat(json.length());
    p.concat("\r\n");
    p.concat("Content-Type: application/x-www-form-urlencoded\r\n\r\n");
    p.concat(json);
    Serial.println("REQUETE:");
    Serial.println(p);
    client.print(p);
    // while (client.connected() || client.available())
    // {
      // if (client.available())
      // {
        // String line = client.readStringUntil('\n');
        // Serial.println(line);
      // }
    // }

    delay(500);
    Serial.println("request sent");
    digitalWrite(0, HIGH);    // green on
    digitalWrite(4, LOW);   // red off
    delay(5000);
    digitalWrite(4, HIGH);    // red on
    digitalWrite(0, LOW);   // green off
  }
}
